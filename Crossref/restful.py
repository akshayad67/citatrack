from CitationSource.restful import CitationSource, utils as util
from Crossref import utils, constant as cs
import json
import sys
import requests


class Crossref(CitationSource):
    def __init__(self):
        super().__init__()
        self.data_source = "Crossref"

    def search_by_id(self, val):
        try:
            response = utils.http_req(cs.API_AUTHOR_ID, data=val) if self.option == 1 else \
                utils.http_req(cs.API_JOURNAL_ID, data=val)
            if response.ok:
                data = json.loads(response.text)
                if self.option == 1 and data["message"]["total-results"] > 0:
                    for item in data["message"]["items"][0]["author"]:
                        if "ORCID" in item and val in item["ORCID"]:
                            return {item["given"] + item["family"]: val}
                else:
                    if data['message'] and data['message']['counts']:
                        return {data['message']['title']: val}
            else:
                raise util.IDNotFound
        except util.IDNotFound:
            print(f"ID {val} not Found")
            sys.exit(1)
        except util.WorkNotFound:
            print(f"0 Works for {val} found")
            sys.exit(1)

    def search_by_name(self, val):
        list_export = []

        try:
            if self.option == 2:
                response = utils.http_req(cs.API_JOURNAL,
                                          data=val,
                                          )
            data = json.loads(response.text)
            if data['status'] != 'ok':
                raise util.NameNotFound
            count_flag = data['message']['total-results']
            if count_flag == 0:
                raise util.NameNotFound
            elif count_flag == 1:
                return {data['message']['items'][0]['title']: data['message']['items'][0]['ISSN'][0]}
            else:
                count_item = 50 if count_flag > 50 else count_flag
                name = "author" if self.option == 1 else "journal"
                print(f"{count_flag} entries available for the {name} \"{val}\" . \n Please refine your search and "
                      f"search via the Author ID. To view the top {count_item} entries please check the "
                      f"{val}.csv file")
                for item in data['message']['items']:
                    if len(item['ISSN']):
                        print(f"{item['title']} :  {item['ISSN'][0]}")
                        if self.option == 1:
                            list_export.append({"Author Name": item['display_name'], 'Author ID': item['id']})
                            print(f"{item['title']} :  {item['ISSN'][0]}")
                            utils.export_file(val + '.csv', cs.csv_author_cols, list_export)
                        else:
                            list_export.append({"Journal Name": item['title'], 'ISSN': item['ISSN'][0]})
                            utils.export_file(val + '.csv', cs.csv_journal_cols, list_export)
                util.pbar.update(100)
                raise util.MultipleAuthorName
        except util.NameNotFound:
            print(cs.NAME_ERROR + val + cs.NOT_FOUND)
            sys.exit(1)
        except util.MultipleAuthorName:
            sys.exit(1)

    def fetch_data(self):
        for key in self.dict_detail:
            url_option = cs.API_JOURNAL_ID if self.option == 2 else cs.API_AUTHOR_ID
            url = url_option + self.dict_detail[
                key] + '/works?filter=from-pub-date:' + self.start_date + ',until-pub-date:' + self.end_date + "&rows=200&cursor=*"
            resp = requests.get(url)
            if resp.ok:
                data = json.loads(resp.text)
                if data['message']['total-results'] > 200:
                    for x in range(int(data['message']['total-results'] / 200) + 1):
                        next_cursor = rem_data['message']['next-cursor'] if x > 0 else data['message']['next-cursor']
                        url1 = 'https://api.crossref.org/journals/' + self.dict_detail[
                            key] + '/works?filter=from-pub-date:' + self.start_date + ',until-pub-date:' + self.end_date + "&rows=200&cursor=" + next_cursor
                        resp1 = requests.get(url1)
                        rem_data = json.loads(resp1.text)
                        data['message']['items'] = data['message']['items'] + rem_data['message']['items']
                self.save_works(data['message']['items'], key)
            else:
                print(resp.text)
        self.compare()

    def save_works(self, data, val):
        for work in data:
            self.dict_works[work["DOI"].lower()].append(val)
            if 'reference' in work:
                self.save_cites_data(work, val)

    def save_cites_data(self, data, val):
        for ref in data["reference"]:
            if 'DOI' in ref:
                self.dict_citation[ref['DOI'].lower()].append(val)
                self.dict_ref[ref['DOI'].lower()].append(data['DOI'])
