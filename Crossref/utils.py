
import requests
import csv


def http_req(request_url, data, cursor='*'):
    url = request_url + data + "&rows=50&cursor=" + cursor
    r = requests.get(url)
    return r


# Export list of author names to a csv file
def export_file(file_name, column_name, csv_ls):
    try:
        with open(file_name, 'w') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=column_name)
            writer.writeheader()
            for data in csv_ls:
                writer.writerow(data)
    except IOError:
        print("I/O error")

