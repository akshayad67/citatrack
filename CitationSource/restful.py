import sys
from CitationSource import utils
from CitationSource import constant as cs
from abc import ABC, abstractmethod
from collections import defaultdict
import re
import copy
from dateutil import parser
import requests_cache

requests_cache.install_cache('journals', backend='sqlite', expire_after=999)

# Abstract Class
class CitationSource(ABC):
    def __init__(self):
        self.option = None
        self.start_date = None
        self.end_date = None
        self.fpath = None
        self.data_source = None
        self.dict_citation = defaultdict(list)
        self.dict_works = defaultdict(list)
        self.dict_ref = defaultdict(list)
        self.dict_detail = defaultdict(list)

    def get_data(self, option, start_date, end_date, fpath):
        try:
            self.option = option
            self.start_date = start_date
            self.end_date = end_date
            self.fpath = fpath
            self.validate_date(start_date)
            self.validate_date(end_date)
            with open(fpath) as f:
                content = (f.read().replace('\n', '')).split(cs.COMMA)
            if option == 1 or option == 2:
                self.get_citation_data(content)
            else:
                raise utils.InvalidOption
        except utils.InvalidOption:
            print(cs.ERROR_OPTION)
            sys.exit(1)
        except FileNotFoundError:
            print(cs.FILE_NOT_FOUND)
            sys.exit(1)

    def validate_date(self, date_string):
        try:
            bool(parser.parse(date_string))
        except ValueError:
            print(cs.INCORRECT_DATE)
            sys.exit(1)

    def get_citation_data(self, values):
        for val in values:
            utils.pbar.update(15 / len(values))
            if self.check_regex(self.option, val):
                self.dict_detail.update(self.search_by_id(val))
            else:
                self.dict_detail.update(self.search_by_name(val))
        self.fetch_data()

    def check_regex(self, option, data):
        if (option == 1 or 2) and re.search(cs.REGEX, data):
            return True
        return False

    @abstractmethod
    def search_by_id(self, val):
        pass

    @abstractmethod
    def search_by_name(self, val):
        pass

    @abstractmethod
    def fetch_data(self):
        pass

    # Function to compare authors
    def compare(self):
        csv_list = []
        export_list = []
        count = 0
        flag = 'Author' if self.option == 1 else 'Journal'
        for key in self.dict_citation:
            if key in self.dict_works:
                count = count + 1
                var = self.dict_ref[key] if key in self.dict_ref else None
                calc_dict = {
                    "No.": count,
                    "Referenced Work": key,
                    flag: self.dict_works[key],
                    "Referenced By " + flag: self.dict_citation[key],
                    'Referenced By Work': var
                }
                csv_list.append(calc_dict)
                export_dict = copy.deepcopy(calc_dict)
                export_dict["Referenced By " + flag] = list(set(self.dict_citation[key]))
                export_list.append(export_dict)
                print("Referenced Work -" + key)
                print(flag, self.dict_works[key])
                print("Referenced By " + flag, list(set(self.dict_citation[key]))),
                print("Referenced By Work", var)
                print("----------------------------------------------------------------------------")
        dict_matrix = self.build_matrix(csv_list)
        matrix = self.build_dict(dict_matrix)
        self.export_bt(dict_matrix)
        self.export_csv(export_list, matrix)

    def build_dict(self, dict_matrix):
        list_authors = []
        i = 0
        for key in dict_matrix:
            j = 0
            for val in dict_matrix[key]:
                list_authors.append({cs.JOURNAL1: key, cs.JOURNAL2: val,
                                     cs.CITATION_COUNT: dict_matrix[key][val]})
                j = j + 1
            i = i + 1
        return list_authors

    def build_matrix(self, data):
        matrix = defaultdict(lambda: defaultdict(float))
        for work in data:
            if self.option == 1:
                values = cs.AUTHOR
                # count = len(work['Author']) * len(work['Referenced By ' + values])
                count = 1
            else:
                values = cs.JOURNAL
                count = 1
            # count = len(list(set(work['authors'] + work['referenced_by'])))
            for author in work[values]:
                for referrer in work[cs.REFERENCED_BY + values]:
                    matrix[referrer][author] += 1.0 / count
        return matrix

    def export_bt(self, matrix):
        csv_col = ['Journal1', 'Journal2', 'Win1', 'Win2']
        ls = []
        test = []
        for i in self.dict_detail:
            for j in self.dict_detail:
                win1 = 0
                win2 = 0
                if i != j and ({j, i} not in test):
                    if matrix[i][j]:
                        win1 = matrix[j][i]
                    if matrix[j][i]:
                        win2 = matrix[i][j]
                    test.append({i, j})
                    ls.append({'Journal1': i, 'Journal2': j,
                               'Win1': win1, 'Win2': win2})
        utils.export_file(self.data_source + '_BT.csv', csv_col, ls)

    def export_csv(self, csv_list, matrix):
        csv_columns = cs.CSV_COLUMNS
        columns = cs.MATRIX_COLUMNS
        csv_file = self.data_source + cs.AUTHOR_FILE_NAME
        csv_file2 = self.data_source + cs.AUTHOR_MATRIX_NAME
        csv_columns_journals = cs.CSV_DETAIL_FILE_NAME
        csv_file_journal = self.data_source + cs.JOURNAL_FILE_NAME
        csv_file2_journal = self.data_source + cs.JOURNAL_MATRIX_NAME
        if self.option == 1:
            utils.export_file(csv_file, csv_columns, csv_list)
            utils.export_file(csv_file2, columns, matrix)
        else:
            utils.export_file(csv_file_journal, csv_columns_journals, csv_list)
            utils.export_file(csv_file2_journal, columns, matrix)
