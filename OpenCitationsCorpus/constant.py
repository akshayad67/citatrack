ERROR_OPTION = "Wrong option! Please choose the option 1 or 2"
API_JOURNAL = "https://api.crossref.org/journals?query="
API_JOURNAL_ID = "https://api.crossref.org/journals/"
JOURNAL_ERROR = "The Journal "
NOT_FOUND = " was not found"
OCC_URL = 'https://opencitations.net/index/coci/api/v1/references/'
csv_author_cols = ['No.', 'Author Name', 'ISSN']
csv_journal_cols = ['No.', 'Journal Name', 'ISSN']
DATA_SOURCE = "OCC"
DOI = "DOI"
CITED = 'cited'
# csv_author_file_name = "author.csv"
# csv_journal_file_name = "journal.csv"
