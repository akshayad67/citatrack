from CitationSource.restful import CitationSource, utils as util
from Crossref.restful import Crossref
from OpenCitationsCorpus import utils, constant as cs
import json
import requests


class OpenCitationsCorpus(Crossref):
    def __init__(self):
        super().__init__()
        self.data_source = cs.DATA_SOURCE

    def save_works(self, data, val):
        for work in data:
            self.dict_works[work[cs.DOI].lower()].append(val)
            self.save_occ_data(work[cs.DOI].lower())

    def save_occ_data(self, key):
        count = 0
        resp_ref = requests.get(cs.OCC_URL + key)
        count = count + 1
        if resp_ref.ok:
            resp_txt = json.loads(resp_ref.text)
            for val in resp_txt:
                self.dict_citation[val[cs.CITED].lower()].append(self.dict_works[key][0])
                self.dict_ref[val[cs.CITED].lower()].append(key)
        else:
            print(resp_ref.text)
