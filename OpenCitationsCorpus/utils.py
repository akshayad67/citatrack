import requests
import csv


def http_req(request_url, data, cursor='*'):
    url = request_url + data + "&rows=50&cursor=" + cursor
    # print(url)
    r = requests.get(url)
    return r


# Export list of author names to a csv file
def export_file(file_name, column_name, csv_ls):
    try:
        with open(file_name, 'w') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=column_name)
            writer.writeheader()
            for data in csv_ls:
                writer.writerow(data)
    except IOError:
        print("I/O error")


class Error(Exception):
    """Base class for other exceptions"""
    pass


class InvalidOption(Error):
    """Raised when the option is wrong"""
    pass


class IDNotFound(Error):
    """Raised when the author's/ journals id was not found"""
    pass


class WorkNotFound(Error):
    """Raised when the author's id was not found"""
    pass


class MultipleAuthorName(Error):
    """Raised when multiple results for author's name was returned"""
    pass


class NameNotFound(Error):
    """Raised when the name was not found"""
    pass
