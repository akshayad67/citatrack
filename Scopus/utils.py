import requests
from Scopus import constant as cs


def http_req(header, request_url, data, cursor='*'):
    url = request_url + data + cs.PAGE + cursor
    print(url)
    r = requests.get(url, headers=header)
    return r
