ERROR_OPTION = "Wrong option! Please choose the option 1 or 2"
API_AUTHOR_NAME = "https://api.elsevier.com/content/search/author?query=authlast"
API_AUTHOR_ID = "https://api.elsevier.com/content/search/scopus?query=AU-ID"
API_JOURNAL_NAME = "https://api.elsevier.com/content/serial/title?title="
API_JOURNAL_ID = "https://api.elsevier.com/content/search/scopus?query="
PAGE = "&count=200&cursor="
csv_author_cols = ['No.', 'Author Name', 'Author ID']
csv_journal_cols = ['No.', 'Journal Name', 'Author ID']
NAME_ERROR = "The name "
AUTHOR_NOT_FOUND = " was not found"
header = "dummy"
header_test = "dum"
DATA_SOURCE = "Scopus"
SEARCH_RESULTS = 'search-results'
TOTAL_RESULTS = 'opensearch:totalResults'
ENTRY = 'entry'
PUBLICATION_NAME = 'prism:publicationName'