from CitationSource.restful import CitationSource, utils as util
from Scopus import constant as cs
from Scopus import utils
import json
import sys
import requests
import time
import copy


class Scopus(CitationSource):
    def __init__(self):
        super().__init__()
        self.data_source = cs.DATA_SOURCE

    # Search by ID
    def search_by_id(self, val):
        try:
            time.sleep(0.15)
            if self.option == 1:
                response = utils.http_req(cs.header, cs.API_AUTHOR_ID,
                                          data="(" + val + ")" + "&field=author"
                                          )
            else:
                response = utils.http_req(cs.header, cs.API_JOURNAL_ID,
                                          data="ISSN(" + val + ")"
                                          )
            data = json.loads(response.text)
            if 'error' in data:
                raise util.IDNotFound
            elif data[cs.SEARCH_RESULTS][cs.TOTAL_RESULTS] == 0:
                raise util.WorkNotFound
            if self.option == 1:
                for auth in data[cs.SEARCH_RESULTS][cs.ENTRY][0]["author"]:
                    if val == auth["authid"]:
                        return {auth["authname"]: val}
            else:
                search_result_entry = data[cs.SEARCH_RESULTS][cs.ENTRY][0]
                if 'prism:issn' in search_result_entry:
                    return {search_result_entry[cs.PUBLICATION_NAME]: search_result_entry[
                        'prism:issn']}
                elif 'prism:eIssn' in search_result_entry:
                    return {search_result_entry['prism:publicationName']: data[cs.SEARCH_RESULTS][cs.ENTRY][0][
                        'prism:eIssn']}
                else:
                    print("Something wrong with ISSN")
        except util.IDNotFound:
            print(f"ID {val} not Found")
            sys.exit(1)
        except util.WorkNotFound:
            print(f"0 Works for {val} found")
            sys.exit(1)

    # Search by OPEN ALEX Name
    def search_by_name(self, val):

        try:
            display_name = None
            time.sleep(0.15)
            if self.option == 1:
                firstname = val.rpartition(' ')[0]
                lastname = val.rpartition(' ')[-1]
                fullname = f"({lastname})%20and%20authfirst({firstname})%20"
                response = utils.http_req(cs.header, cs.API_AUTHOR_NAME,
                                          data=fullname,
                                          )
            else:
                response = utils.http_req(cs.header, cs.API_JOURNAL_NAME,
                                          data=val,
                                          )
            data = json.loads(response.text)
            if self.option == 1:
                display_name = self.search_author_name(data, val)
            else:
                display_name = self.search_journal_name(data, val)
            return display_name
        except util.NameNotFound:
            print(cs.NAME_ERROR + val + cs.AUTHOR_NOT_FOUND)
            sys.exit(1)
        except util.MultipleAuthorName:
            sys.exit(1)

    def search_author_name(self, author_data, val):
        list_export = []
        count_flag = int(author_data[cs.SEARCH_RESULTS]['opensearch:totalResults'])
        if 'error' in author_data:
            raise util.NameNotFound
        elif count_flag == 0:
            raise util.NameNotFound
        elif count_flag == 1:
            search_result = author_data[cs.SEARCH_RESULTS][cs.ENTRY][0]
            fname = search_result['preferred-name']['surname'] + " " + \
                    search_result['preferred-name']['given-name']
            dc_id = search_result['dc:identifier'].replace("AUTHOR_ID", '') if "AUTHOR_ID" in search_result['dc:identifier'] else search_result['dc:identifier']
            return {fname: dc_id}
        else:
            count_item = 200 if count_flag > 200 else count_flag
            name = "author" if self.option == 1 else "journal"
            print(f"{count_flag} entries available for the {name} \"{val}\" . \n Please refine your search and "
                  f"search via the Author ID. To view the top {count_item} entries please check the "
                  f"{val}.csv file")
            for item in author_data[cs.SEARCH_RESULTS][cs.ENTRY]:
                print(f"{item['preferred-name']['given-name']}  {item['preferred-name']['surname']} :  {item['dc:identifier']}")
                if self.option == 1:
                    list_export.append({"Author Name": item['preferred-name']['given-name'] + " " + item['preferred-name']['surname'], "Author ID" : item['dc:identifier']})
                    util.export_file(val + '.csv', cs.csv_author_cols, list_export)
            util.pbar.update(100)
            raise util.MultipleAuthorName

    def search_journal_name(self, data, val):
        list_export = []
        count_flag = len(data['serial-metadata-response'][cs.ENTRY])
        if 'error' in data:
            raise util.NameNotFound
        elif count_flag == 0:
            raise util.NameNotFound
        elif count_flag == 1:
            prismid = 'prism:issn' if 'prism:issn' in data['serial-metadata-response'][cs.ENTRY][0] else 'prism:eIssn'
            journName = {data['serial-metadata-response'][cs.ENTRY][0]['dc:title']: data['serial-metadata-response'][cs.ENTRY][0][prismid]}
            return journName
        else:
            count_item = 200 if count_flag > 200 else count_flag
            name = "author" if self.option == 1 else "journal"
            print(f"{count_flag} entries available for the {name} \"{val}\" . \n Please refine your search and "
                  f"search via the Author ID. To view the top {count_item} entries please check the "
                  f"{val}.csv file")
            for item in data['results']:
                print(f"{item['display_name']} :  {item['id']}")
                list_export.append({"Journal Name": item['display_name'], 'Author ID': item['id']})
                util.export_file(val + '.csv', cs.csv_journal_cols, list_export)
            util.pbar.update(100)
            raise util.MultipleAuthorName

    def http_req(self, key, url, cursor='*'):
        query = "ISSN(" + self.dict_detail[
            key] + ")" + "%20AND%20PUBYEAR%20>%20" + str(int(self.start_date) - 1) + "%20AND%20PUBYEAR%20<%20" + str(int(self.end_date) + 1)
        response = utils.http_req(cs.header, url,
                                  query, cursor
                                  )
        data = json.loads(response.text)
        return data

    # API to filter out works in a date range
    def fetch_data(self):
        self.fetch_author() if self.option == 1 else self.fetch_journal()
        print(" Workdata len", len(self.dict_works))
        self.save_reference()
        self.compare()
        util.pbar.update(55)
        util.pbar.close()

    def fetch_journal(self):
        url = cs.API_JOURNAL_ID
        for key in self.dict_detail:
            data = []
            resp_txt = self.http_req(key, url)
            count = int(resp_txt[cs.SEARCH_RESULTS]['opensearch:totalResults'])
            print("total results ", count)
            if count > 0:
                data.extend(resp_txt['search-results'][cs.ENTRY])
            while count > 200:
                time.sleep(0.15)
                new_url = resp_txt[cs.SEARCH_RESULTS]['link'][2]['@href']
                resp_new = requests.get(new_url, headers=cs.header)
                resp_txt = json.loads(resp_new.text)
                data.extend(resp_txt[cs.SEARCH_RESULTS][cs.ENTRY])
                count = count - 200
            print("len with update", len(data))
            self.save_works(data, key)

    def fetch_author(self):
        for key in self.dict_detail:
            data = []
            query = "(" + self.dict_detail[
                key] + ")" + "%20AND%20PUBYEAR%20>%20" + str(int(self.start_date) - 1) + "%20AND%20PUBYEAR%20<%20" + str(int(self.end_date) + 1)
            resp = utils.http_req(cs.header, cs.API_AUTHOR_ID,
                                      query)
            resp_txt = json.loads(resp.text)
            count = int(resp_txt['search-results']['opensearch:totalResults'])
            print("total results ", count)
            if count > 0:
                data.extend(resp_txt[cs.SEARCH_RESULTS][cs.ENTRY])
            while count > 200:
                time.sleep(0.15)
                new_url = resp_txt[cs.SEARCH_RESULTS]['link'][2]['@href']
                resp_new = requests.get(new_url, headers=cs.header)
                resp_txt = json.loads(resp_new.text)
                data.extend(resp_txt[cs.SEARCH_RESULTS][cs.ENTRY])
                count = count - 200
            self.save_works(data, key)

    def save_works(self, data, val):
        for work in data:
            if "eid" in work:
                self.dict_works[work["eid"]].append(val)
                time.sleep(0.01)

    def save_reference(self):
        time.sleep(0.15)
        for key in self.dict_works:
            query = 'REFEID(' + key + ')'
            resp = utils.http_req(cs.header, cs.API_JOURNAL_ID,
                                  data=query, cursor='*'
                                  )
            if resp.ok:
                data_resp = json.loads(resp.text)
                if len(data_resp[cs.SEARCH_RESULTS][cs.ENTRY]) > 200:
                    print("count > 200")
                for ref in data_resp[cs.SEARCH_RESULTS][cs.ENTRY]:
                    if "eid" in ref:
                        self.dict_citation[ref["eid"]].append(self.dict_works[key][0])
                        self.dict_ref[ref["eid"]].append(key)
                    else:
                        print(ref)

            else:
                print("Response is not okay")

    def compare(self):
        csv_list = []
        export_list = []
        count = 0
        flag = 'Author' if self.option == 1 else 'Journal'
        for key in self.dict_citation:
            if key in self.dict_works:
                count = count + 1
                var = self.dict_ref[key] if key in self.dict_ref else None
                calc_dict = {
                    "No.": count,
                    "Referenced Work": var,
                    flag: self.dict_citation[key],
                    "Referenced By " + flag:  self.dict_works[key],
                    'Referenced By Work': key
                }
                csv_list.append(calc_dict)
                export_dict = copy.deepcopy(calc_dict)
                export_dict["Referenced By " + flag] = list(set(self.dict_citation[key]))
                export_list.append(export_dict)
                print("Referenced Work -", var)
                print(flag, list(set(self.dict_citation[key])))
                print("Referenced By " + flag, self.dict_works[key]),
                print("Referenced By Work", key)
                print("----------------------------------------------------------------------------")
        dict_matrix = self.build_matrix(csv_list)
        matrix = self.build_dict(dict_matrix)
        self.export_bt(dict_matrix)
        self.export_csv(export_list, matrix)
