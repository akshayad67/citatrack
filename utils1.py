


class Error(Exception):
    """Base class for other exceptions"""
    pass


class InvalidOption(Error):
    """Raised when the option is wrong"""
    pass


class AuthorIDNotFound(Error):
    """Raised when the author's id was not found"""
    pass


class AuthorWorkNotFound(Error):
    """Raised when the author's id was not found"""
    pass


class JournalIDNotFound(Error):
    """Raised when the author's id was not found"""
    pass


class JournalWorkNotFound(Error):
    """Raised when the author's id was not found"""
    pass


class MultipleAuthorName(Error):
    """Raised when multiple results for author's name was returned"""
    pass


class AuthorNameNotFound(Error):
    """Raised when the author's name was not found"""
    pass


class JournalNameNotFound(Error):
    """Raised when the author's name was not found"""
    pass
