OPENALEXAPI_AUTHOR = "https://api.openalex.org/authors?filter=display_name.search:"
OPENALEXAPI_VENUE = "https://api.openalex.org/sources?filter=display_name.search:"
OPENALEXAPI_AUTHOR_ID = "https://api.openalex.org/authors?filter=openalex_id:"
OPENALEXAPI_VENUE_ID = "https://api.openalex.org/sources?filter=openalex_id:"
ERROR_OPTION = "Wrong option! Please choose the option 1 or 2"
AUTHOR_WORKS = "https://api.openalex.org/works?filter=author.id:"
VENUES_WORKS2 = "https://api.openalex.org/works?filter=openalex_id:"
VENUES_WORKS = "https://api.openalex.org/works?filter=locations.source.id:"
PAGE = "&per-page=200&cursor="
NAME_ERROR = "The name "
AUTHOR_NOT_FOUND = " was not found"
PATH = "/home"
csv_author_cols = ['No.', 'Author Name', 'Author ID']
csv_journal_cols = ['No.', 'Journal Name', 'Journal ID']
REGURL = "^.https://openalex.org/+"
REGAUTHOR = r"https://openalex\.org/A\d+"
REGVENUE = r"https://openalex\.org/S\d+"
OPENALEX = "OA"
ERROR = "error"
META = 'meta'
COUNT = 'count'
RESULTS = 'results'
DISPLAY_NAME = 'display_name'
ID = 'id'
AUTHOR = "author"
JOURNAL = "journal"
NEXT_CURSOR = 'next_cursor'
REF_WORK = "referenced_works"
CSV = '.csv'
JOURNAL_NAME = "Journal Name"
AUTHOR_ID = 'Author ID'
AUTHOR_NAME = 'Author Name'
JOURNAL_ID = 'Journal ID'
# csv_author_file_name = "author.csv"
# csv_journal_file_name = "journal.csv"
