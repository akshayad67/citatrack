from CitationSource.restful import CitationSource
from OpenAlex import utils, constant as cs
from CitationSource import utils as util
import json
import sys
import re


class OpenAlex(CitationSource):
    def __init__(self):
        super().__init__()
        self.data_source = cs.OPENALEX

    # Function to check regular expression for recognizing ID and authors name
    def check_regex(self, option, data):
        if option == 1 and re.search(cs.REGAUTHOR, data):
            return True
        elif option == 2 and re.search(cs.REGVENUE, data):
            return True
        return False

    # Search by OPEN ALEX ID
    def search_by_id(self, val):
        try:
            if self.option == 1:
                response = utils.http_req(cs.OPENALEXAPI_AUTHOR_ID,
                                          data=val
                                          )
            else:
                response = utils.http_req(cs.OPENALEXAPI_VENUE_ID,
                                          data=val
                                          )
            data = json.loads(response.text)
            if cs.ERROR in data:
                raise util.IDNotFound
            elif data[cs.META][cs.COUNT] == 0:
                raise util.WorkNotFound
            return {data[cs.RESULTS][0][cs.DISPLAY_NAME]: data[cs.RESULTS][0][cs.ID]}
        except util.IDNotFound:
            print(f"ID {val} not Found")
            sys.exit(1)
        except util.WorkNotFound:
            print(f"0 Works for {val} found")
            sys.exit(1)

    # Search by OPEN ALEX Name
    def search_by_name(self, val):
        list_export = []

        try:
            if self.option == 1:
                response = utils.http_req(cs.OPENALEXAPI_AUTHOR,
                                          data=val,
                                          )
            else:
                response = utils.http_req(cs.OPENALEXAPI_VENUE,
                                          data=val,
                                          )
            data = json.loads(response.text)
            count_flag = data[cs.META][cs.COUNT]
            if cs.ERROR in data:
                raise util.NameNotFound
            elif count_flag == 0:
                raise util.NameNotFound
            elif count_flag == 1:
                return {data[cs.RESULTS][0][cs.DISPLAY_NAME]: data[cs.RESULTS][0][cs.ID]}
            else:
                count_item = 200 if count_flag > 200 else count_flag
                name = cs.AUTHOR if self.option == 1 else cs.JOURNAL
                print(f"{count_flag} entries available for the {name} \"{val}\" . \n Please refine your search and "
                      f"search via the Author ID. To view the top {count_item} entries please check the "
                      f"{val}.csv file")
                for item in data[cs.RESULTS]:
                    print(f"{item[cs.DISPLAY_NAME]} :  {item[cs.ID]}")
                    if self.option == 1:
                        list_export.append({cs.AUTHOR_NAME: item[cs.DISPLAY_NAME], cs.AUTHOR_ID: item[cs.ID]})
                        util.export_file(val + cs.CSV, cs.csv_author_cols, list_export)
                    else:
                        list_export.append({cs.JOURNAL_NAME: item[cs.DISPLAY_NAME], cs.JOURNAL_ID: item[cs.ID]})
                        util.export_file(val + cs.CSV, cs.csv_journal_cols, list_export)
                util.pbar.update(100)
                raise util.MultipleAuthorName
        except util.NameNotFound:
            print(cs.NAME_ERROR + val + cs.AUTHOR_NOT_FOUND)
            sys.exit(1)
        except util.MultipleAuthorName:
            sys.exit(1)

    def http_req(self, key, url, cursor='*'):
        query = self.dict_detail[
                    key] + ",from_publication_date:" + self.start_date + ",to_publication_date:" + self.end_date
        response = utils.http_req(url,
                                  query, cursor
                                  )
        data = json.loads(response.text)
        return data

    # API to filter out works in a date range
    def fetch_data(self):
        url = cs.AUTHOR_WORKS if self.option == 1 else cs.VENUES_WORKS
        for key in self.dict_detail:
            data = []
            resp = self.http_req(key, url)
            count = resp[cs.META][cs.COUNT]
            data.extend(resp[cs.RESULTS])
            while count > 200:
                cursor = resp[cs.META][cs.NEXT_CURSOR]
                resp = self.http_req(key, url, cursor)
                data.extend(resp[cs.RESULTS])
                count = count - 200
            self.save_works(data, key)
        self.compare()
        util.pbar.update(55)
        util.pbar.close()

    def save_works(self, data, val):
        for work in data:
            self.dict_works[work[cs.ID]].append(val)
            self.save_cites_data(work, val)

    def save_cites_data(self, data, val):
        for ref in data[cs.REF_WORK]:
            self.dict_citation[ref].append(val)
            if data[cs.ID] not in self.dict_ref[ref]:
                self.dict_ref[ref].append(data[cs.ID])

