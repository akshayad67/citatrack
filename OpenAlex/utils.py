import requests
from OpenAlex import constant as cs


# Make a call via OPENALEX API
def http_req(request_url, data, cursor='*'):
    url = request_url + data + cs.PAGE + cursor
    r = requests.get(url)
    return r
