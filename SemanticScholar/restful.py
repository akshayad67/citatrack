from Crossref.restful import Crossref
import json
import requests
import time
from collections import defaultdict
from SemanticScholar import constant as cs


class SemanticScholar(Crossref):
    def __init__(self):
        super().__init__()
        self.data_source = cs.DATA_SOURCE
        self.dict_cr_works = defaultdict(list)

    def save_works(self, data, val):
        for work in data:
            self.dict_cr_works[work[cs.DOI].lower()].append(val)
            self.save_ss_data(work[cs.DOI].lower())

    def save_ss_data(self, works):
        count = 0
        count = count + 1
        time.sleep(0.2)
        resp_ref = requests.get(
            cs.SS_URL + works + cs.PARAMETER)
        resp_txt = json.loads(resp_ref.text)
        if cs.ERROR not in resp_txt:
            self.dict_works[resp_txt[cs.PAPER_ID]].append(self.dict_cr_works[works][0])
            for val in resp_txt[cs.REFERENCES]:
                if val[cs.PAPER_ID]:
                    self.dict_citation[val[cs.PAPER_ID]].append(self.dict_cr_works[works][0])
                    self.dict_ref[val[cs.PAPER_ID].lower()].append(works)
        else:
            print(resp_txt)
