ERROR_OPTION = "Wrong option! Please choose the option 1 or 2"
API_JOURNAL = "https://api.crossref.org/journals?query="
API_JOURNAL_ID = "https://api.crossref.org/journals/"
API_AUTHOR_ID = "https://api.semanticscholar.org/graph/v1/author/search?query="
JOURNAL_ERROR = "The Journal "
NOT_FOUND = " was not found"
DOI = "DOI"
DATA_SOURCE = "SS"
SS_URL = 'https://api.semanticscholar.org/graph/v1/paper/'
PARAMETER = '?fields=title,references.title,referenceCount'
PAPER_ID = 'paperId'
csv_author_cols = ['No.', 'Author Name', 'ISSN']
csv_journal_cols = ['No.', 'Journal Name', 'ISSN']
ERROR = 'error'
REFERENCES = 'references'
# csv_author_file_name = "author.csv"
# csv_journal_file_name = "journal.csv"
