#!/usr/bin/env python
from setuptools import setup, find_packages

from citationMatrixApi import VERSION

install_requires = [
    'requests>=2.11.1',
    'numpy'
]

tests_require = []

setup(
    name="citationMatrix",
    version=VERSION,
    description="Library that implements the endpoints of the OPENALEX API",
    author="Akshaya Dharmaraj",
    author_email="akshayad67@gmail.com",
    maintainer="Akshaya Dharmaraj",
    maintainer_email="akshayad67@gmail.com",
    url="http://github.com/fabiobatalha/crossrefapi",
    packages=find_packages(),
    include_package_data=True,
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: Developers",
        "Programming Language :: Python :: 3",
    ],
    dependency_links=[],
    tests_require=tests_require,
    test_suite='tests',
    install_requires=install_requires
)
